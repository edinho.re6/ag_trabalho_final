# AG_Trabalho_Final

Software para Posicionamento de Gateways em Redes LoRa: Uma Solução baseada em Algoritmo Genético

Uma rede LoRa (Long Range) se baseia em um conjunto de sensores (endnodes) enviando dados para um gateway usando uma determinada frequência como 915Mhz e partir deste enviar esses dados para um servidor através de conexão com a internet para poder ser usado em uma aplicação como sistemas web ou aplicativos. Com isso, este trabalho tem como objetivo calcular e definir as posições de uma quantidade mínima de gateways afim de se ter todos os endnodes dentro do raio de cobertura.

Para isso será desenvolvido um Algoritmo Genético para calcular a posição dos gateways a partir de uma área determinada, a perda e qualidade da transmissão em relação a distância e o raio da cobertura do gateway a fim de se ter a maior cobertura de endnodes possíveis, se uma posição de gateway não conseguir fazer a cobertura de todos os endnodes, o processamento será refeito a fim de se ter uma nova posição de gateway para os endnodes restantes.

para o cenário de testes será usada a área da Universidade Federal do Pará - Campus Guamá

Para os modelos de propagação foram selecionados o modelo Close In e Float Intercept. o Close in para o cálculo de propagação em larga escala implementado o calculo com multifrequencia tem que ser feito antes o cálculo de perda de propagação em espaço livre (Free Space Path Loss - FSPL)

FSPL(f,d_0)=10〖log〗_10 ((4〖πd〗_0)/λ)^2

onde f, d_0, e λ representam a frequência, a distância entre transmissor e receptor de um metro e o comprimento de onda respectivamente 

após o calculo do FSPL é realizado do close in

P_L^CI (f,d)[dB]=FSPL+10n〖log〗_10 (d/d_0 )+X_σ

onde d, X_σ, e n representam, respectivamente, distância entre o transmissor e o receptor, uma variável aleatória gaussiana com média 0 e desvio padrão  e o expoente de perda do ambiente n.

Já o modelo de proragação Float Intercept é dado pela equação

P_L^FI (d)[dB]=α+10β〖log〗_10 (d)+X_σ^FI

onde α, β, e X_σ^FI representam, respectivamente, a interceptação flutuante em dB, a inclinação da linha, e o sombreamento gaussiano com média zero. Tal que X_σ^FI descreve as flutuações do sinal em larga escala sobre a perda média de propagação com a distância

Pseudocódigo da aplicação AG
Entrada
 Matriz de pontos a serem cobertos (endnodes)

Processamento
 se for a primeira iteração gerar uma população de cromossomos de gateways candidatos
	
 Calcular o valor de perda a partir dos modelos de propagação em dBm para todos os gateways candidatos em relação a todos os pontos de endnodes

Condicional 1
 se o total de gerações for alcançado
	 Selecionar o gateway candidato com a mínima perda a partir dos gateways candidatos
 senão
	 Atualizar a população de cromossomos (cruzamento e mutação)
		
Condicional 2
 Se todos os endnodes são cobertos pelo gateway candidato 
	 Guardar o gateway candidato na matriz de gateways selecionados
 senão
	 Podar a matriz de pontos a serem cobertos mantendo somente os pontos que não foram atendidos pelo gateway candidato
        	Guardar o gateway candidato na matriz de gateways selecionados
Fim
